# Deploy-viallure-s3fs

https://docs.aws.amazon.com/cli/latest/reference/s3/sync.html

## Projeto voltado para melhoria na pipeline de deploy da viallure

Hoje a aplicação do magento mantém EC2:1 admin - m4.xlarge2 nodes - c4.xlarge

Sendo os 2 nodes atachado a um Load Balancer, onde cada node tem 2 regras em seu contrab, qual faz umn rsync a cada 5 minutos para a instancia do viallure-admin

**Sincronizacao Mandic - rodar a partir dos nodes**
```
*/5 ** ** ** ** deploy /usr/bin/time -v rsync -e 'ssh -p 2269' -ur 172.31.10.219:/wwwroot/production/shared/media /wwwroot/production/shared --exclude=media/catalog/product/cache/* >> /var/log/rivensync.log   2>&1
** ** ** ** ** deploy rsync -ur -e 'ssh -p 2269' /wwwroot/production/shared/media/catalog/product/cache 172.31.10.219: >> /var/log/rivensync.log 2>&1
```

Processo de Deploy atual esta usando jenkins com 3 gatilhos Deploy, Backup e Rollback.

Os 3 gatilhos estão referenciando a um playbook do ansible que realizado o processo de deploy.

### Problemas atuais: 

a cada deploy realizado precisamos comentar a linha do crontab que realiza um sync entre admins e as nodes dificultando o processo, fazemos:

A ideia é realizarmos uma melhoria na sua pipelina de deploy

Cenário Viallure antigo(usando rsync nas nodes):


